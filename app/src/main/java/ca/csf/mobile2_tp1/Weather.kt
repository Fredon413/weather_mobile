package ca.csf.mobile2_tp1

import android.os.Parcel
import android.os.Parcelable

data class Weather(
    var city: String,
    var temperatureInCelsius: Int,
    var type: WeatherType ) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readInt(),
        WeatherType.valueOf(parcel.readString()!!)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(city)
        parcel.writeInt(temperatureInCelsius)
        parcel.writeString(type.name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Weather> {
        override fun createFromParcel(parcel: Parcel): Weather {
            return Weather(parcel)
        }

        override fun newArray(size: Int): Array<Weather?> {
            return arrayOfNulls(size)
        }
    }
}

enum class WeatherType {
    SUNNY,
    PARTLY_SUNNY,
    CLOUDY,
    RAIN,
    SNOW
}