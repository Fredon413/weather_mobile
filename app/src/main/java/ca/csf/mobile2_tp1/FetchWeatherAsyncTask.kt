package ca.csf.mobile2_tp1

import android.os.AsyncTask
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException

class FetchWeatherAsyncTask(
    private val successCall: SuccessCall,
    private val connectionError: connectionError,
    private val serverError: ServerError) : AsyncTask<String, Void, Weather>() {
                                    private var isConnectionError: Boolean = false
                                    private var isServerError: Boolean = false
                                    private var weather: Weather? = Weather("Default city name", 99, WeatherType.SUNNY)


    override fun doInBackground(vararg params: String): Weather? {
        val url = "$BASE_URL${params[0]}"

        val client = OkHttpClient()
        val request = Request.Builder()
            .url(url)
            .build()

        try {
            val response = client.newCall(request).execute()

            if (!response.isSuccessful) {
                isServerError = true
            } else {
                val responseBody = response.body()?.string()
                val mapper = jacksonObjectMapper()

                if (responseBody != null)
                    weather = mapper.readValue<Weather>(responseBody)
            }
        } catch (e: IOException) {
            isConnectionError = true
            e.printStackTrace()
        }
        return weather
    }

    override fun onPostExecute(weather: Weather) {
        when {
            isConnectionError -> connectionError()
            isServerError -> serverError()
            else -> successCall(weather)
        }
    }
}

private const val BASE_URL = "http://10.200.15.63:8080/api/v1/weather/"

typealias SuccessCall = (Weather) -> Unit
typealias connectionError = () -> Unit
typealias ServerError = () -> Unit