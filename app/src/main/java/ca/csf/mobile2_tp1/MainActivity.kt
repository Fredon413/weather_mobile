package ca.csf.mobile2_tp1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.constraintlayout.widget.Group

class MainActivity : AppCompatActivity() {
    private lateinit var searchbarEditText: EditText
    private lateinit var cityTextView: TextView
    private lateinit var temperatureInCelsiusTextView: TextView
    private lateinit var errorTextView: TextView
    private lateinit var weatherTypeImageView: ImageView
    private lateinit var retryButton: Button
    private lateinit var searchProgressBar: ProgressBar

    private lateinit var connectionProblemGroup: Group
    private lateinit var searchSuccessGroup: Group

    private var weather: Weather? = null
    private var isFetchingWeather: Boolean = false
    private var connectionProblemGroupActive: Boolean = false
    private var searchSuccessGroupActive: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        searchbarEditText = findViewById(R.id.searchBarEditText)
        cityTextView = findViewById(R.id.cityTextView)
        temperatureInCelsiusTextView = findViewById(R.id.temperatureInCelsiusTextView)
        errorTextView = findViewById(R.id.errorTextView)
        weatherTypeImageView = findViewById(R.id.weatherTypeImageView)
        retryButton = findViewById(R.id.retryButton)

        connectionProblemGroup = findViewById(R.id.connectionProblemGroup)
        searchSuccessGroup = findViewById(R.id.searchSuccessGroup)
        searchProgressBar = findViewById(R.id.searchProgressbar)

        searchbarEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE && searchbarEditText.text.toString().isNotBlank()) {
                beginAsyncTask()
                false
            } else
                true
        }

        retryButton.setOnClickListener {
            beginAsyncTask()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putParcelable(WEATHER, weather)

        outState.putString(CITY_NAME, searchbarEditText.text.toString())
        outState.putString(MSG_ERROR, errorTextView.text.toString())
        outState.putBoolean(IS_FETCHING_WEATHER, isFetchingWeather)
        outState.putBoolean(CONNECTION_PROBLEM_GROUP_ACTIVE, connectionProblemGroupActive)
        outState.putBoolean(SEARCH_SUCCESS_GROUP_ACTIVE, searchSuccessGroupActive)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        weather = savedInstanceState.getParcelable(WEATHER)

        searchbarEditText.setText(savedInstanceState.getString(CITY_NAME))
        errorTextView.text = savedInstanceState.getString(MSG_ERROR)
        isFetchingWeather = savedInstanceState.getBoolean(IS_FETCHING_WEATHER)
        connectionProblemGroupActive = savedInstanceState.getBoolean(CONNECTION_PROBLEM_GROUP_ACTIVE)
        searchSuccessGroupActive = savedInstanceState.getBoolean(SEARCH_SUCCESS_GROUP_ACTIVE)

        when {
            isFetchingWeather -> beginAsyncTask()
            connectionProblemGroupActive -> setConnectionProblemGroupVisibility(true)
            searchSuccessGroupActive -> {
                readWeather()
                setWeatherFetchedGroupVisibility(true)
            }
        }
    }

    private fun beginAsyncTask() {
        prepareView()

        val fetchWeatherAsyncTask = FetchWeatherAsyncTask(
            this::onWeatherFetched,
            this::onConnectionError,
            this::onServerError
        )
        fetchWeatherAsyncTask.execute(searchbarEditText.text.toString())
    }

    private fun prepareView() {
        setWeatherFetchedGroupVisibility(false)
        setConnectionProblemGroupVisibility(false)
        setWeatherProgressBarVisibility(true)
    }

    private fun onWeatherFetched(weather: Weather) {
        this.weather = weather
        readWeather()
        setWeatherProgressBarVisibility(false)
        setWeatherFetchedGroupVisibility(true)
    }

    private fun readWeather() {
        cityTextView.text = weather?.city
        temperatureInCelsiusTextView.text = getString(R.string.celsiusSymbol, weather?.temperatureInCelsius.toString())

        readWeatherType()
    }

    private fun readWeatherType() {
        when {
            weather?.type == WeatherType.SUNNY -> weatherTypeImageView.setImageResource(R.drawable.ic_sunny)
            weather?.type == WeatherType.PARTLY_SUNNY -> weatherTypeImageView.setImageResource(R.drawable.ic_partly_sunny)
            weather?.type == WeatherType.CLOUDY -> weatherTypeImageView.setImageResource(R.drawable.ic_cloudy)
            weather?.type == WeatherType.RAIN -> weatherTypeImageView.setImageResource(R.drawable.ic_rain)
            weather?.type == WeatherType.SNOW -> weatherTypeImageView.setImageResource(R.drawable.ic_snow)
        }
    }

    private fun onConnectionError() {
        setWeatherProgressBarVisibility(false)
        errorTextView.setText(R.string.connectionErrorTextView)
        setConnectionProblemGroupVisibility(true)
    }

    private fun onServerError() {
        setWeatherProgressBarVisibility(false)
        errorTextView.setText(R.string.serverErrorTextView)
        setConnectionProblemGroupVisibility(true)
    }

    private fun setWeatherProgressBarVisibility(visible: Boolean) {
        if (visible) {
            searchProgressBar.visibility = View.VISIBLE
            isFetchingWeather = true
        } else {
            searchProgressBar.visibility = View.GONE
            isFetchingWeather = false
        }
    }

    private fun setWeatherFetchedGroupVisibility(visible: Boolean) {
        if (visible) {
            searchSuccessGroup.visibility = View.VISIBLE
            searchSuccessGroupActive = true
        } else {
            searchSuccessGroup.visibility = View.GONE
            searchSuccessGroupActive = false
        }
    }

    private fun setConnectionProblemGroupVisibility(visible: Boolean) {
        if (visible) {
            connectionProblemGroup.visibility = View.VISIBLE
            connectionProblemGroupActive = true
        } else {
            connectionProblemGroup.visibility = View.GONE
            connectionProblemGroupActive = false
        }
    }
}

private const val CITY_NAME: String = "CITY_NAME"
private const val WEATHER: String = "WEATHER"
private const val IS_FETCHING_WEATHER: String = "IS_FETCHING_WEATHER"
private const val MSG_ERROR: String = "MSG_ERROR"
private const val CONNECTION_PROBLEM_GROUP_ACTIVE: String = "CONNECTION_PROBLEM_GROUP_ACTIVE"
private const val SEARCH_SUCCESS_GROUP_ACTIVE: String = "SEARCH_SUCCESS_GROUP_ACTIVE"
